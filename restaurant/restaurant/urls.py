from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from apps.restaurants import urls as urls_restaurants

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    # importar URLs de otra app
    url(r'^', include(urls_restaurants)),

    # url para permitir el login con facebook
    url('', include('social.apps.django_app.urls', namespace='social')),
    # para hacer el logout
    # url('', include('django.contrib.auth.urls', namespace='auth')),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),

    # esta URL sirve para poder visualizar las imagenes
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', 
    		{'document_root': settings.MEDIA_ROOT,}
    	),
]
