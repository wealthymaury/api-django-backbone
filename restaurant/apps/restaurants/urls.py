from django.conf.urls import include, url
from rest_framework import routers

from .views import IndexView
from .viewsets import RestaurantViewSet, CategoryViewSet, PaymentViewSet, CityViewSet

router = routers.DefaultRouter()
router.register(r'restaurants', RestaurantViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'payments', PaymentViewSet)
router.register(r'cities', CityViewSet)

urlpatterns = [
    url(r'^$', IndexView.as_view(), name="home"),
    url(r'^api/', include(router.urls)),
]
