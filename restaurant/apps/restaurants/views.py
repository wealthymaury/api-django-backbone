from django.shortcuts import render
from django.views.generic import TemplateView

from .models import Category, Payment, City, Restaurant

class IndexView(TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		context['categories'] = Category.objects.all()
		context['payments'] = Payment.objects.all()
		context['cities'] = City.objects.all()
		restaurants = Restaurant.objects.all()
		tips = [ restaurant.tip_set.all().count() for restaurant in restaurants ]
		context['restaurants'] = zip(restaurants, tips)

		return context

class DetalleView(TemplateView):
	template_name = 'detalle.html'

from django.contrib.auth.models import User
from django.http import HttpResponse
import json

#esto es una request AJAX de tipo POST
def guardar_tip(request):
	if request.is_ajax():
		user = User.objects.get(username = reques.POST['username'])
		restaurant = Restaurant.objects.get(id = request.POST['id'])
		Tip.objects.create(user = user, restaurant = restaurant, content = request.POST['content'])
		response = {
			'user' : user.username,
			'content' : request.POST['content'],
		}

		#esto es viejo ya no se usa
		return HttpResponse(json.dumps(response), content_type = 'application/json')