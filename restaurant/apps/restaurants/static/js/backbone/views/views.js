var app = app || {};

//Los filtros estan incompletos, no sirven

app.mainView = Backbone.View.extend({
	el : '#app',
	events : {
		'keyup #buscador' : 'buscarRestaurant',
		// 'click #ciudad a' : 'selectCiudad',
		// 'click .categoria' : 'selectCategoria',
		// 'click .pago' : 'selectPago',
	},
	initialize : function(){
		app.restaurantsCollection.on('add', this.agregarRestaurantFiltro)
		app.restaurantsCollection.fetch();
		this.llegoFinal();
	},
	selectCiudad : function(e){
		window.ciudad = $(e.target).attr('id');
		app.restaurantCiudad = Backbone.Model.extend({
			urlRoot : 'api/cities/' + window.ciudad + '/restaurants/'
		});
		var restaurantsCiudades = Backbone.Collection.extend({
			model : app.restaurantCiudad,
			url : 'api/cities/' + window.ciudad + '/restaurants' 
		});
		app.restaurantsCiudadesCollection = new restaurantsCiudades();

		if(window.categoria > 0 && window.pago>0){
			this.filtroCategoriaCiudadPago();
		}else if(window.categoria>0 && window.pago<=0){
			this.filtroCategoriaCiudad();
		}else if(window.categoria<=0 && window.pago>0){
			this.filtroCiudadPago();
		}else if(window.categoria<=0 && window.pago<=0){
			this.filtroCiudad();
		}
	},
	selectCategoria : function(e){
		window.categoria = $(e.target).attr('id');
		app.restaurantCategoria = Backbone.Model.extend({
			urlRoot : 'api/categories/' + window.categoria + '/restaurants/'
		});
		var restaurantsCategorias = Backbone.Collection.extend({
			model : app.restaurantCategoria,
			url : 'api/categories/' + window.categoria + '/restaurants' 
		});
		app.restaurantsCategoriasCollection = new restaurantsCategorias();
	},
	selectPago : function(e){
		window.pago = $(e.target).attr('id');
		app.restaurantPago = Backbone.Model.extend({
			urlRoot : 'api/payments/' + window.pago + '/restaurants/'
		});
		var restaurantsPagos = Backbone.Collection.extend({
			model : app.restaurantPago,
			url : 'api/payments/' + window.pago + '/restaurants' 
		});
		app.restaurantsPagosCollection = new restaurantsPagos();
	},
	filtroCiudad : function(){
		var self = this;
		app.restaurantsCiudadesCollection.fetch({
			success : function(ciudades){
				ciudades.each(self.agregarRestaurant, self);
			}
		});
	},
	filtroCategoria : function(){
		var self = this;
		app.restaurantsCategoriasCollection.fetch({
			success : function(categorias){
				categorias.each(self.agregarRestaurant, self);
			}
		});
	},
	filtroPago : function(){
		var self = this;
		app.restaurantsPagosCollection.fetch({
			success : function(pagos){
				pagos.each(self.agregarRestaurant, self);
			}
		});
	},
	filtroCategoriaCiudad : function(){
		var self = this;
		app.restaurantsCategoriasCollection.fetch({
			success : function(categorias){
				categorias.each(function(categoria){
					app.restaurantsCiudadesCollection.fetch({
						success: function(ciudades){
							ciudades.each(function(ciudad){
								if(categoria.get('id')==ciudad.get('id')){
									self.agregarRestaurant(categoria);
								}
							});
						}
					});
				});
			}
		});
	},
	filtroCategoriaPago: function(){
		var self = this;
		app.restaurantsCategoriasCollection.fetch({
			success:function(categorias){
				categorias.each(function(categoria){
					app.restaurantsPagosCollection.fetch({
						success: function(pagos){
							pagos.each(function(pago){
								if(ciudad.get('id')==pago.get('id')){
									self.agregarRestaurant(ciudad);
								}
							});
						}
					})
				});
			}
		});
	},
	filtroCategoriaCiudadPago : function(){
		var self = this;
		app.restaurantsCategoriasCollection.fetch({
			success : function(categorias){
				categorias.each(function(categoria){
					app.restaurantsCiudadesCollection.fetch({
						success: function(ciudades){
							ciudades.each(function(ciudad){
								app.restaurantsPagosCollection.fetch({
									success : function(pagos){
										pagos.each(function(pago){
											if(
												(categoria.get('id')==pago.get('id')) &&
												(categoria.get('id')==ciudad.get('id')) &&
												(ciudad.get('id')==pago.get('id'))
											){
												self.agregarRestaurant(categoria);
											}
										});
									}
								});
							});
						}
					});
				});
			}
		});
	},
	agregarRestaurant : function(modelo){
		var view = new app.restaurantView({model : modelo});
		$('.list-group').append(view.render().fadeIn('slow'));
	},
	agregarRestaurantFiltro : function(modelo, collection, xhr){
		if((window.cantidad >= modelo.get('id')) && window.cantidad-5 < modelo.get('id')){	
			window.mainView.agregarRestaurant(modelo);
		}
	},
	buscarRestaurant : function(){
		window.state = true;
		var cadBuscador = $('#buscador').val().toLowerCase();
		var resultados = app.restaurantsCollection.filter(function(model){
			var cadModel = model.get('name').substring(0, cadBuscador.length).toLowerCase();
			if(
				(cadBuscador === cadModel) &&
				(cadBuscador.length === cadModel.length) &&
				(cadBuscador.length !== 0) &&
				(cadModel.length !== 0)
			){
				return model;
			}else if(cadModel.length === 0 && cadBuscador.length === 0){ 
				window.state = false;
				return model;
			}
		});
		this.agregarFiltro(resultados);
	},
	agregarFiltro : function(resultados){
		this.$('.list-group').html('');
		if(window.state === false){
			resultados = resultados.filter(function(model){
				if(window.cantidad >= model.get('id')){
					return model;
				}
			});
		}
		resultados.forEach(this.agregarRestaurant, this);
	},
	llegoFinal : function(){
		var self = this;
		$(window).scroll(function(){
			if($(window).height() + $(window).scrollTop() == $(document).height()){
				if(window.state === false){
					window.cantidad = window.cantidad + 5;
					app.restaurantsCollection.each(self.agregarRestaurantFiltro, self);
				}
			}
		});
	}
});

app.restaurantView = Backbone.View.extend({
	template : _.template($('#tpl-restaurant').html()),
	render : function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this.$el;
	}
});