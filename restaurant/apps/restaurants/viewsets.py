from rest_framework import viewsets
# este decorador sirve para poder crear una url dentro de otra
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from .models import Restaurant, Category, City, Payment, Establishment, Tip
from .serializers import RestaurantSerializer, CategorySerializer, CitySerializer, PaymentSerializer, TipSerializer

# Extiende de ReadOnlyModelViewSet y solo permite hacer metodos GET
class RestaurantViewSet(viewsets.ReadOnlyModelViewSet):
	model = Restaurant
	queryset = Restaurant.objects.all()
	serializer_class = RestaurantSerializer
	paginate_by = 10
	filter_fields = ('id',)

	@detail_route()
	def tips(self, request, pk=None):
		tips = Tip.objects.filter(restaurant__pk = pk)
		serializer = TipSerializer(tips, many=True)
		return Response(serializer.data)

class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
	model = Category
	queryset = Category.objects.all()
	serializer_class = CategorySerializer
	paginate_by = 10

	# restaurants seria el nombre de la nueva URL
	# pk seria el ID que viene en la URL de la categoria
	@detail_route()
	def restaurants(self, request, pk=None):
		# restaurant__category__pk es como moverte asi: Establishment.Restaurant.Category.Id
		# distinct es para que en la consulta no haya restaurants repetidos
		establishments = Establishment.objects.filter(restaurant__category__pk=pk)#.distinct('restaurant__name')
		# por cada establecimiento dentro de establishments quiero traer el restaurant y meterlo en la lista
		restaurants = [establishment.restaurant for establishment in establishments]
		# defino como mandare los datos
		serializer = RestaurantSerializer(restaurants, many=True)
		return Response(serializer.data)

class CityViewSet(viewsets.ReadOnlyModelViewSet):
	model = City
	queryset = City.objects.all()
	serializer_class = CitySerializer

	@detail_route()
	def restaurants(self, request, pk=None):
		establishments = Establishment.objects.filter(city__pk=pk)#.distinct('restaurant__name')
		restaurants = [establishment.restaurant for establishment in establishments]
		serializer = RestaurantSerializer(restaurants, many=True)
		return Response(serializer.data)

class PaymentViewSet(viewsets.ReadOnlyModelViewSet):
	model = Payment
	queryset = Payment.objects.all()
	serializer_class = PaymentSerializer

	@detail_route()
	def restaurants(self, request, pk=None):
		establishments = Establishment.objects.filter(restaurant__payment__pk=pk)#.distinct('restaurant__name')
		restaurants = [establishment.restaurant for establishment in establishments]
		serializer = RestaurantSerializer(restaurants, many=True)
		return Response(serializer.data)


