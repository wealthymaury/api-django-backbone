from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

class City(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

class Payment(models.Model):
    pay = models.CharField(max_length=50)

    def __unicode__(self):
    	return self.pay

class Restaurant(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=200)
    image = models.ImageField(upload_to='fotos', blank=True, null=True)
    payment = models.ManyToManyField(Payment)
    category = models.ManyToManyField(Category)

    def __unicode__(self):
    	return self.name

class Establishment(models.Model):
	address = models.CharField(max_length=50)
	restaurant = models.ForeignKey(Restaurant)
	city = models.ForeignKey(City)

	def __unicode__(self):
		return self.address

class Tip(models.Model):
	content = models.TextField(max_length=200)
	restaurant = models.ForeignKey(Restaurant)
	user = models.ForeignKey(User)
